package es.upm.dit.apsv.traceconsumer_2.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import es.upm.dit.apsv.traceconsumer_2.Repository.TraceRepository;
import es.upm.dit.apsv.traceconsumer_2.model.Trace;
import es.upm.dit.apsv.traceconsumer_2.Repository.TransportationOrderRepository;


import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import es.upm.dit.apsv.traceconsumer_2.model.TransportationOrder;


@RestController
public class TraceController {

  private final TraceRepository tr;
  private TransportationOrderRepository repository;
  public TraceController(TraceRepository tr) {
    this.tr = tr;
  }

  @GetMapping("/traces")
  List<Trace> all() {
    return (List<Trace>) tr.findAll();
  }

  @PostMapping("/traces")
  Trace newTraze(@RequestBody Trace newTraze) {
    return tr.save(newTraze);
  }

  @GetMapping("/traces/{id}")
  Trace one(@PathVariable String id) {
    return tr.findById(id).orElseThrow();
  }

  @PutMapping("/traces/{id}")
  Trace replaceTraze(@RequestBody Trace newTrace, @PathVariable String id) {
    return tr.findById(id).map(Trace -> {
      Trace.setTraceId(newTrace.getTraceId());
      Trace.setTruck(newTrace.getTruck());
      Trace.setLastSeen(newTrace.getLastSeen());
      Trace.setLat(newTrace.getLat());
      Trace.setLng(newTrace.getLng());
      return tr.save(Trace);
    }).orElseGet(() -> {
      newTrace.setTraceId(id);
      return tr.save(newTrace);
    });
  }
  @DeleteMapping("/traces/{id}")
  void deleteTraze(@PathVariable String id) {
    tr.deleteById(id);
  }
  
  @PostMapping("/orders")
  TransportationOrder newOrder(@RequestBody TransportationOrder newOrder) {
    return repository.save(newOrder);
  }
  
  @GetMapping("/orders/{truck}")
  ResponseEntity<TransportationOrder> getByTruck(@PathVariable String truck) {
    Optional<TransportationOrder> ot = repository.findById(truck);
    if (ot.isPresent()) 
      return new ResponseEntity<>(ot.get(), HttpStatus.OK);
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  @PutMapping("/orders")
  ResponseEntity<TransportationOrder> update(@RequestBody TransportationOrder updatedOrder) {
    TransportationOrder to = repository.save(updatedOrder);
    if (to == null) 
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    return new ResponseEntity<>(to, HttpStatus.OK);
    //       return repository.save(updatedOrder).orElseThrow();
  }

  @DeleteMapping("/orders/{truck}")
  void deleteOrder(@PathVariable String truck) {
    repository.deleteById(truck);
  }    
}
