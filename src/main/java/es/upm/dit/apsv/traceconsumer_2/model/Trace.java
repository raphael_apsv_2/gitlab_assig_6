package es.upm.dit.apsv.traceconsumer_2.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Trace {
    
    @Id
    private String traceid;
    private String truck;
    private long lastSeen;
    private double lat;
    private double lng;

    public Trace() {
    }
    public Trace(String traceid, String truck, long lastSeen,
                 double lat, double lng) {
        this.traceid = traceid;
        this.truck = truck;
        this.lastSeen = lastSeen;
        this.lat = lat;
        this.lng = lng;
    }
    
    public String getTraceId() {
        return traceid;
    }

    public void setTraceId(String traceid) {
        this.traceid = traceid;
    }
    public String getTruck() {
        return truck;
    }

    public void setTruck(String truck) {
        this.truck = truck;
    }
    
    public long getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(long lastSeen) {
        this.lastSeen =  lastSeen;
    }
    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat ;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }
}